package ir.ac.shahreza.computer.spec.homework3;

import java.util.Scanner;

public class Main {
    public static boolean isValid(String str) {
        if (str.length() == 0 || str.equals("ab"))
            return true;
        else if (str.length() < 2)
            return false;

        else {
            String first = str.substring(0,1);
            String last = str.substring(str.length()-1);
            String mid = str.substring(1, str.length()-1);

            if (first.equals("a") && last.equals("b"))
                return isValid(mid);
            else
                return false;
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = null;

        while (true) {
            System.out.println("\tEnter the code or enter :q to exit:");
            input = scanner.nextLine();

            if (input.equals(":q"))
                break;

            else if (isValid(input))
                System.out.println("\tTrue");
            else
                System.out.println("\tSyntax Error");
        }
    }
}
